<%-- Listing of all of the taglibs that we reference in this application --%>
<%-- Struts provided Taglibs --%>

<%-- Common error page for all JSPs --%>
<%@ taglib uri="/WEB-INF/tld/c.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/fmt.tld" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/fn.tld" prefix="fn" %>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<script type="text/javascript">
    var ctx = "${ctx}";
</script>
 