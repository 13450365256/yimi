<%@ include file="/WEB-INF/jsp/common/taglibs.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script>
	var jobType = "${jobInfo.jobType}";
</script>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>文章添加--layui后台管理模板</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" href="${ctx}/js/lib/admin/layui/css/layui.css" media="all" />
	<link rel="stylesheet" href="http://at.alicdn.com/t/font_qk9d838rioei2j4i.css" media="all" />
	<link rel="stylesheet" href="${ctx}/js/lib/admin/css/order.css" media="all" />
</head>
<body class="childrenBody">
	<form class="layui-form" style="width:100%;">
		<div class="layui-form-item">
			<p style="color:red;">提示:*号为必填项</p>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">任务名称(*)</label>
			<div class="layui-input-block">
				<input type="text" class="layui-input jobName" lay-verify="required" placeholder="请输入任务名称" value="${jobInfo.jobName}">
				<input type="hidden" class="dataId" value="${jobInfo.id}"/>
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">任务类型(*)</label>
			<div class="layui-input-inline">
				<select class="jobType" lay-verify="required">
					<option value="">请选择</option>
					<c:forEach var="jobType" items="${jobTypeList}">
						<option value="${jobType}">${jobType.desc}</option>
					</c:forEach>
				</select>
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">任务组名称(*)</label>
			<div class="layui-input-block">
				<input type="text" class="layui-input jobGroupName" lay-verify="required" placeholder="请输入任务组名称" value="${jobInfo.jobGroupName}">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">触发器名称(*)</label>
			<div class="layui-input-block">
				<input type="text" class="layui-input triggerName" lay-verify="required" placeholder="请输入触发器名称" value="${jobInfo.triggerName}">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">触发器组名称(*)</label>
			<div class="layui-input-block">
				<input type="text" class="layui-input triggerGroupName" lay-verify="required" placeholder="请输入触发器组名称" value="${jobInfo.triggerGroupName}">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">时间规则(*)</label>
			<div class="layui-input-block">
				<input type="text" class="layui-input cron" lay-verify="required" placeholder="请输入时间规则" value="${jobInfo.cron}">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">定时任务接口包名(*)</label>
			<div class="layui-input-block">
				<input type="text" class="layui-input packageName" lay-verify="required" placeholder="请输入包名" value="${jobInfo.packageName}">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">备注</label>
			<div class="layui-input-block">
				<input type="text" class="layui-input remark" placeholder="备注" value="${jobInfo.remark}">
			</div>
		</div>
		<div class="layui-form-item">
			<div class="layui-input-block">
				<button class="layui-btn" lay-submit="" lay-filter="addJobInfo">立即提交</button>
				<button class="layui-btn layui-btn-primary" lay-filter="reset">重置</button>
		    </div>
		</div>
	</form>
	<script type="text/javascript" src="${ctx}/js/lib/admin/layui/layui.js"></script>
	<script type="text/javascript" src="${ctx}/js/lib/admin/js/addJobInfo.js"></script>
</body>
</html>