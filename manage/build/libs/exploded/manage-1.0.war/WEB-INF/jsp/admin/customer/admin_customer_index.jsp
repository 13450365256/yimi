<%@ include file="/WEB-INF/jsp/common/taglibs.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>客户查询</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="${ctx}/js/lib/admin/layui/css/layui.css?time=${time}" media="all" />
    <link rel="stylesheet" href="http://at.alicdn.com/t/font_qk9d838rioei2j4i.css" media="all" />
    <link rel="stylesheet" href="${ctx}/js/lib/admin/css/order.css?time=${time}" media="all" />
</head>
<body class="childrenBody">
<blockquote class="layui-elem-quote order_search" style="width:auto;">
        <div class="layui-form-item layui-form layui-form-pane">

            <div class="layui-inline">
                <select class="queryType">
                    <option value="customerName">客户姓名</option>
                    <option value="customerNo">客户编号</option>
                    <option value="customerPhone">客户电话</option>
                    <option value="regUserId">客户所属人</option>
                    <option value="unit">所属部门</option>
                    <option value="team">所属分行</option>
                </select>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">搜索条件</label>
                <div class="layui-input-block">
                    <input type="text" value="" placeholder="搜索条件" class="layui-input searchContent">
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">经纪ID</label>
                <div class="layui-input-inline">
                    <input type="text" value="" placeholder="经纪ID" class="layui-input userId">
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">城市</label>
                <div class="layui-input-inline">
                    <input type="text" value="" placeholder="城市" class="layui-input city">
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">区域</label>
                <div class="layui-input-inline">
                    <input type="text" value="" placeholder="区域" class="layui-input askArea">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">客户类型</label>
                <div class="layui-input-inline">
                    <input type="text" value="" placeholder="客户类型" class="layui-input vipType">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">需求类型</label>
                <div class="layui-input-inline">
                    <input type="text" value="" placeholder="需求类型" class="layui-input needType">
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">物业类型</label>
                <div class="layui-input-inline">
                    <input type="text" value="" placeholder="物业类型" class="layui-input propertyType">
                </div>
            </div>
            <hr/>
            <div class="layui-inline">
                <label class="layui-form-label">户型</label>
                <div class="layui-input-inline">
                    <input type="text" value="" placeholder="户型-低" class="layui-input roomLow">
                </div>
                <div class="layui-input-inline">
                    <input type="text" value="" placeholder="户型-高" class="layui-input roomHigh">
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">面积</label>
                <div class="layui-input-inline">
                    <input type="text" value="" placeholder="面积-低" class="layui-input squareLow">
                </div>
                <div class="layui-input-inline">
                    <input type="text" value="" placeholder="面积-高" class="layui-input squareHigh">
                </div>
            </div>


            <div class="layui-inline">
                <label class="layui-form-label">预算(售)</label>
                <div class="layui-input-inline">
                    <input type="text" value="" placeholder="预算(售)-低" class="layui-input moneyLow">
                </div>
                <div class="layui-input-inline">
                    <input type="text" value="" placeholder="预算(售)-高" class="layui-input moneyHigh">
                </div>
            </div>


            <div class="layui-inline">
                <label class="layui-form-label">预算(售)</label>
                <div class="layui-input-inline">
                    <input type="text" value="" placeholder="预算(租)-低" class="layui-input rentMoneyLow">
                </div>
                <div class="layui-input-inline">
                    <input type="text" value="" placeholder="预算(租)-高" class="layui-input rentMoneyHigh">
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">排序</label>
                <div class="layui-input-inline">
                    <select class="followStatus" lay-verify="required">
                        <option value="*">全部</option>
                        <option value="FOLLOWED">最近已跟进</option>
                        <option value="NOT_FOLLOWED">最近未跟进</option>
                    </select>
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">查询耗时</label>
                <div class="layui-input-inline">
                    <input type="text" value="" class="layui-input queryspeed" disabled>
                </div>
            </div>

            <a class="layui-btn search_btn">查询</a>
        </div>
</blockquote>
<div class="layui-form links_list">
    <table class="layui-table">
        <colgroup>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
        </colgroup>
        <thead>
        <tr>
            <th>客户编号</th>
            <th>客户名称</th>
            <th>客户电话</th>
            <th>区域</th>
            <th>购房需求</th>
            <th>物业类型</th>
            <th>客户类型</th>
            <th>客户状态</th>
            <th>售(预算)</th>
            <th>租(预算)</th>
            <th>面积</th>
            <th>户型</th>
            <th>标签</th>
            <th>所属人</th>
            <th>部门</th>
            <th>所属分行</th>
            <th>最后跟进时间</th>
        </tr>
        </thead>
        <tbody class="links_content"></tbody>
    </table>
</div>
<div id="page"></div>
<script type="text/javascript" src="${ctx}/js/lib/admin/layui/layui.js"></script>
<script type="text/javascript" src="${ctx}/js/lib/admin/js/customer.js?time=${time}"></script>
</body>
</html>