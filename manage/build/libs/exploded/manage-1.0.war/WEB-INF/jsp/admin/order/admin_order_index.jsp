<%@ include file="/WEB-INF/jsp/common/taglibs.jsp" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>订单管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="${ctx}/js/lib/admin/layui/css/layui.css?time=${time}" media="all" />
    <link rel="stylesheet" href="http://at.alicdn.com/t/font_qk9d838rioei2j4i.css" media="all" />
    <link rel="stylesheet" href="${ctx}/js/lib/admin/css/order.css?time=${time}" media="all" />
</head>
<body class="childrenBody">
<blockquote class="layui-elem-quote order_search">
    <%--<input type="file" name="file1" lay-type="file" class="layui-upload-file"  id="uploadYunFanOrderExcel">--%>
        <input type="file" lay-type="file" name="file" class="layui-upload-file" >
        <div>
            <label>文档：</label><span id="fileNameSpan" ></span><input id="filePathHidden" type="hidden" />
        </div>

    <div class="layui-inline">
        <a class="layui-btn layui-btn-danger export_yi_mi_order_excel">生成发货单</a>
    </div>
        <div class="layui-inline">
            <a class="layui-btn layui-btn-danger export_yi_mi_order_excel_gift">生成发货单(带赠品)</a>
        </div>

</blockquote>
<div class="layui-form links_list">
    <table class="layui-table">
        <colgroup>
            <col>
        </colgroup>
        <thead>
        <tr>
            <th>key</th>
        </tr>
        </thead>
        <tbody class="links_content"></tbody>
    </table>
</div>
<div id="page"></div>
<%--<script type="text/javascript" src="${ctx}/js/lib/admin/layui/layui.js"></script>--%>
<script src="${ctx}/js/lib/admin/layui/layui.js" charset="utf-8"></script>
<script type="text/javascript" src="${ctx}/js/lib/admin/js/order.js?time=${time}"></script>

</body>
</html>