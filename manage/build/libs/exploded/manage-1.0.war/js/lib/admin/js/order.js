/**
 * Created by zq on 2017-6-23.
 */
layui.config({
    base : ctx+"/js/lib/admin/js/"
}).use(['form','layer','jquery','laypage','upload'],function() {
    var form = layui.form(),
        layer = parent.layer === undefined ? layui.layer : parent.layer,
        laypage = layui.laypage,
        $ = layui.jquery,
        upload = layui.upload;


    $("body").on("click", ".export_yi_mi_order_excel", function () {  //启动任务
        if($("#fileNameSpan").text()==""){
            layer.msg("请先上传文档");
            return;
        }
        layer.form
        layer.confirm('是否导出excel？', {icon: 3, title: '提示信息'}, function (index) {


            var index = layer.msg('导出excel中，请稍候', {icon: 16, time: false, shade: 0.8});
            $.ajax({
                url: ctx + "/admin/order/exportYiMiOrderExcel.do",
                type: "get",
                dataType: "json",
                data:{
                    path:$("#filePathHidden").val()
                },

                success: function (data) {
                    $("#filePathHidden").val(null)
                    $("#fileNameSpan").text("")
                    layer.close(index);
                    //layer.msg(data.msg);
                    var path = data.path
                    if(path != null) {
                        location.href = path;
                    }
                },
                error:function (data) {
                    $("#fileNameSpan").text("")
                    $("#filePathHidden").val(null)
                    layer.close(index);
                    layer.msg("发生错误");

                }
            })
        });

    })

    $("body").on("click", ".export_yi_mi_order_excel_gift", function () {  //启动任务
        if($("#fileNameSpan").text()==""){
            layer.msg("请先上传文档");
            return;
        }
        layer.form
        layer.confirm('是否导出excel？', {icon: 3, title: '提示信息'}, function (index) {


            var index = layer.msg('导出excel中，请稍候', {icon: 16, time: false, shade: 0.8});
            $.ajax({
                url: ctx + "/admin/order/exportYiMiOrderExcel.do",
                type: "get",
                dataType: "json",
                data:{
                    path:$("#filePathHidden").val(),
                    giftNum:'NTXG3'
                },

                success: function (data) {
                    $("#filePathHidden").val(null)
                    $("#fileNameSpan").text("")
                    layer.close(index);
                    //layer.msg(data.msg);
                    var path = data.path
                    if(path != null) {
                        location.href = path;
                    }
                },
                error:function (data) {
                    $("#fileNameSpan").text("")
                    $("#filePathHidden").val(null)
                    layer.close(index);
                    layer.msg("发生错误");

                }
            })
        });

    })


    upload({
        url: ctx + "/admin/order/uploadYunFanOrderExcel.do",
        ext: 'xls',
        type: 'file',
        before: function (input) {
            //返回的参数item，即为当前的input DOM对象
            console.log('文件上传中');
            console.log(input);
        }
        , success: function (res,input) {
            console.log('上传完毕');
            console.log(res);
            $("#fileNameSpan").html("<img width='20px' height='20px' src='/yimi-manage/images/icon/excel.jpg' />" + res.data.fileName)
            $("#filePathHidden").val(res.data.resultSrc)
            //alert($("#filePathHidden").val())
            //layer.msg(res.msg)
        }
    });
    /*layui.upload({
        url: '' //上传接口
        ,success: function(res){ //上传成功后的回调
            console.log(res)
        }
    });

    layui.upload({
        elem: '#uploadYunFanOrderExcel' //绑定元素
        , url: '/upload/' //上传接口
        , success: function (res) {
            //上传完毕回调
            alert('上传成功！')
        }
        , error: function () {
            //请求异常回调
            alert('上传报错！')
        }
    });*/



})
