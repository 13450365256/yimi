package com.yimi.manage.system;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Administrator on 2018/4/12.
 */
public class YiMiApplicationContext {

    private static ApplicationContext context;



    public static ApplicationContext getApplicationContext(){
        if(YiMiApplicationContext.context == null){
            YiMiApplicationContext.context = new ClassPathXmlApplicationContext("classpath:config/applicationContext.xml");
        }
        return YiMiApplicationContext.context;
    }

}
