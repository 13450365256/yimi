package com.yimi.manage.constant;

/**
 * Created by Liuhuitao on 2017/12/22.
 */
public class NuoTaiConstant {

    public static String[] YIMI_ORDER_EXCEL_TITLE = new String[]{"交易号","买家昵称","买家留言","卖家备注","收货人","省"
            ,"市","区","收货地址","手机号码","固定电话","快递","快递费用","货到付款","条形码","销售单价","数量","代收款"
            ,"付款时间"};

    public static String NUO_TAI_EXCEL_PATH = ManageConstant.EXCEL_PATH + "nuoTai/";

    public static String NUO_TAI_PRICE_EXCEL_PATH = NUO_TAI_EXCEL_PATH + "price/";

    public static String NUO_TAI_ORDERS_EXCEL_PATH = NUO_TAI_EXCEL_PATH + "orders/";

    public static String NUO_TAI_YI_MI_ORDERS_EXCEL_PATH = NUO_TAI_ORDERS_EXCEL_PATH + "yiMiOrders/";

    public static String NUO_TAI_YUN_FAN_ORDERS_EXCEL_PATH = NUO_TAI_ORDERS_EXCEL_PATH + "yunFanOrders/";




    public static String WEB_NUO_TAI_EXCEL_PATH = ManageConstant.WEB_EXCEL_PATH + "nuoTai/";

    public static String WEB_NUO_TAI_PRICE_EXCEL_PATH = WEB_NUO_TAI_EXCEL_PATH + "price/";

    public static String WEB_NUO_TAI_ORDERS_EXCEL_PATH = WEB_NUO_TAI_EXCEL_PATH + "orders/";

    public static String WEB_NUO_TAI_YI_MI_ORDERS_EXCEL_PATH = WEB_NUO_TAI_ORDERS_EXCEL_PATH + "yiMiOrders/";

    public static String WEB_NUO_TAI_YUN_FAN_ORDERS_EXCEL_PATH = WEB_NUO_TAI_ORDERS_EXCEL_PATH + "yunFanOrders/";
}
