package com.yimi.manage.controller;

import com.yimi.manage.service.NuoTaiService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2018/4/12.
 */
@Controller
@RequestMapping("/adminNuoTaiController")
public class AdminNuoTaiController {

    @Resource
    NuoTaiService nuoTaiService;

    @RequestMapping("/buildNuoTaiPriceRedis.do")
    public void buildNuoTaiPriceRedis() {
        try {
            nuoTaiService.buildPriceRedis();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
