package com.yimi.manage.controller;

import com.yimi.manage.service.NuoTaiService;
import com.yimi.manage.utils.redis.RedisStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created by Administrator on 2017/12/21.
 */
@Controller
@RequestMapping(value = "/testController")
public class TestController {

    @Resource
    NuoTaiService nuoTaiService;

    private Logger logger = LogManager.getLogger(this.getClass());

    @RequestMapping(value="/doTest.do")
    @ResponseBody
    public void doTest(){
        File file = new File("C:\\Users\\Administrator\\Desktop\\订单信息25.xls");
        if(file.exists()) {
            try {
                nuoTaiService.buildYimiOrderExcel(file,null);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            System.out.println();
        }
    }

    @RequestMapping(value="/testRedis.do")
    @ResponseBody
    public void testRedis(){
        RedisStringUtils.save("name","TG");
    }

    @RequestMapping(value="/testAppContext.do")
    @ResponseBody
    public void testAppContext(String path) throws Exception{
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:config/applicationContext.xml");
        org.springframework.core.io.Resource resource = context.getResource(path);
        System.out.println(resource.getFile().getAbsolutePath());


    }
}
