package com.yimi.manage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Description：后台管理页面
 * @ClassName：AdminManageController
 * @author：ZQ
 * @date：2017-6-23
 */

@Controller
@RequestMapping("/admin")
public class AdminManageController {

    @RequestMapping("/admin_index.do")
    public ModelAndView toIndex(ModelAndView modelAndView){
        modelAndView.addObject("time",System.currentTimeMillis());
        modelAndView.setViewName("/admin/index");
        return modelAndView;
    }

    @RequestMapping("/admin_main.do")
    public ModelAndView toMain(ModelAndView modelAndView){
        modelAndView.setViewName("/admin/main");
        return modelAndView;
    }

    @RequestMapping("/admin_order_index.do")
    public ModelAndView toOrder(ModelAndView modelAndView){
        modelAndView.setViewName("/admin/order/admin_order_index");
        return modelAndView;
    }

    @RequestMapping("/admin_job_index.do")
    public ModelAndView toJobInfoIndex(ModelAndView modelAndView){
        modelAndView.addObject("time",System.currentTimeMillis());
        modelAndView.setViewName("/admin/order/admin_job_index");
        return modelAndView;
    }
}
