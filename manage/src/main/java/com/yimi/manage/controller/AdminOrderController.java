package com.yimi.manage.controller;

import com.alibaba.fastjson.JSONObject;
import com.yimi.manage.constant.NuoTaiConstant;
import com.yimi.manage.service.NuoTaiService;
import com.yimi.manage.system.YiMiApplicationContext;
import com.yimi.manage.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Liuhuitao on 2017/12/25.
 */
@Controller
@RequestMapping("/admin/order")
public class AdminOrderController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    NuoTaiService nuoTaiService;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @RequestMapping("/exportYiMiOrderExcel.do")
    @ResponseBody
    public Map exportYiMiOrderExcel(String path,String giftNum){
        String exportPath = null;
        try {
            exportPath = nuoTaiService.buildYimiOrderExcel(new File(path),giftNum);
        }catch (Exception e){
            e.printStackTrace();
        }
        Map rsMap = new HashMap<>();
        rsMap.put("msg","成功");
        if(StringUtils.isNotempty(exportPath)) {
            rsMap.put("path", exportPath);
        }
        return rsMap;
    }

    @RequestMapping("/uploadYunFanOrderExcel.do")
    @ResponseBody
    public String uploadYunFanOrderExcel(HttpServletRequest request,
                                         HttpServletResponse response){
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            String fileName = null;
            String resultSrc = null;
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
            // 文件保存目录路径
            String savePath = NuoTaiConstant.NUO_TAI_YUN_FAN_ORDERS_EXCEL_PATH;
//            savePath = "K:\\project\\yimi\\manage\\src\\main\\resources\\excel\\nuoTai\\orders";
            org.springframework.core.io.Resource savePathResource = YiMiApplicationContext.getApplicationContext().getResource(savePath);
            File saveFile = savePathResource.getFile();
            String saveAbsolutePath = null;


            if(!savePathResource.exists() && !saveFile.exists()){
                if(!saveFile.isDirectory()){
                    saveFile.mkdirs();
                }

            }
            saveAbsolutePath = saveFile.getAbsolutePath();
            System.out.println("saveAbsolutePath:" + saveAbsolutePath);

            response.setContentType("text/html; charset=UTF-8");
            // 检查目录
//            File uploadDir = new File(savePath);
            /*if (!uploadDir.isDirectory()) {
                // 如果不存在，创建文件夹
                if (!uploadDir.exists()) {
                    uploadDir.mkdirs();
                }
            }*/

            String dateStr = sdf.format(new Date());
            if(saveAbsolutePath != null) {
                saveAbsolutePath += ("/" + dateStr);
                File dirFile = new File(saveAbsolutePath);
                if (!dirFile.exists()) {
                    dirFile.mkdirs();
                }
                // 此处是直接采用Spring的上传
                for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
                    MultipartFile mf = entity.getValue();
                    String fileFullname = mf.getOriginalFilename();
                    fileFullname = fileFullname.replace('&', 'a');
                    fileFullname = fileFullname.replace(',', 'b');
                    fileFullname = fileFullname.replace('，', 'c');
                    fileName = fileFullname;
                    // 扩展名
                    String fileExt = fileFullname.substring(fileFullname.lastIndexOf(".") + 1).toLowerCase();

                    resultSrc = saveAbsolutePath + "/云返诺泰订单-" + dateStr + "." + fileExt;
                    System.out.println("===================resultSrc:" + resultSrc);
                    File uploadFile = new File(resultSrc);
                    try {
                        FileCopyUtils.copy(mf.getBytes(), uploadFile);

                    } catch (IOException e) {
                        result.put("code", 200);
                        result.put("msg", "上传失败");
                        e.printStackTrace();
                    }
                }
            }

            result.put("code", 0);
            result.put("msg", "上传成功");
            Map dataMap = new HashMap<>();
            dataMap.put("fileName",fileName);
            dataMap.put("resultSrc",resultSrc);
            result.put("data", dataMap);
            return JSONObject.toJSONString(result);
            // 上传结束
        }catch (Exception e){
            e.printStackTrace();
            result.put("code", -1);
            result.put("msg", "上传失败");
            result.put("data", e.getMessage());
            return JSONObject.toJSONString(new Date());
        }
    }
}
