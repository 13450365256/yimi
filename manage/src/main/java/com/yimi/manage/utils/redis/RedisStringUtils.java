package com.yimi.manage.utils.redis;

import org.apache.log4j.Logger;
import org.springframework.data.redis.core.RedisTemplate;
import redis.clients.jedis.*;
import redis.clients.util.Pool;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @描述: Redis工具类.
 */
public class RedisStringUtils {

	private static Logger logger = Logger.getLogger(RedisStringUtils.class);

	/** 默认缓存时间 */
	public static final int DEFAULT_CACHE_ONE_HOUR = 60 * 60 * 1;// 单位秒 设置成一个钟
	public static final int CACHE_TWO_HOUR = 60 * 60 * 2;// 单位秒
	public static final int CACHE_FROUR_HOUR = 60 * 60 * 4;// 单位秒
	public static final int CACHE_TEN_HOUR = 60 * 60 * 10;// 单位秒
	public static final int CACHE_ONE_DAY = 60 * 60 * 24;// 单位秒
	public static final int CACHE_TWO_DAY = 60 * 60 * 24 * 2;// 单位秒
	public static final int CACHE_ONE_WEEK = 60 * 60 * 24 * 7;// 单位秒
	public static final int CACHE_ONE_MONTH = 60 * 60 * 24 * 30;// 单位秒
	public static final int CACHE_TWO_MONTH = 60 * 60 * 24 * 30 * 2;// 单位秒
	public static final int CACHE_THREE_MONTH = 60 * 60 * 24 * 30 * 3;// 单位秒

	/** 连接池 **/
	private static Pool<Jedis> jedisPool;

	public void setJedisPool(Pool<Jedis> jedisPool) {
		RedisStringUtils.jedisPool = jedisPool;
	}

	public static Pool<Jedis> getJedisPool() {
		return jedisPool;
	}

	private static RedisTemplate redisTemplate;

	public static RedisTemplate getRedisTemplate() {
		return redisTemplate;
	}

	public void setRedisTemplate(RedisTemplate redisTemplate) {
		RedisStringUtils.redisTemplate = redisTemplate;
	}

	/*static {
		if(RedisStringUtils.jedisPool == null){
			Set<String> sentinels = new HashSet<String>();
			sentinels.add("172.16.8.111:26379");
			sentinels.add("10.10.5.19:26379");
			RedisStringUtils.jedisPool  = new JedisSentinelPool("mymaster", sentinels);
		}
	}*/
	/**
	 * 释放redis资源
	 * 
	 * @param jedis
	 */
	public static void releaseResource(Jedis jedis) {
		if (jedis != null) {
			jedis.close();
		}
	}

	/**
	 * 删除Redis中的所有key
	 * @throws Exception
	 */
	public static void flushAll() {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.flushAll();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache清空失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}
	/**
	 * 匹配key
	 * 以pattern结尾的key
	 * @throws Exception
	 */
	public static Set<String> keys_l(String pattern) {
		Jedis jedis = null;
		Set<String> set = null;
		try {
			jedis = jedisPool.getResource();
			set = jedis.keys("*" + pattern);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache清空失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return set;
	}
	/**
	 * 匹配key
	 * 以pattern开头的key
	 * @param pattern 要匹配的key部分
	 * @throws Exception
	 */
	public static Set<String> keys_r(String pattern) {
		Jedis jedis = null;
		Set<String> set = null;
		try {
			jedis = jedisPool.getResource();
			set = jedis.keys(pattern + "*");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache清空失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return set;
	}

	/**
	 * 拿到缓存中包含pattern的key
	 *
	 * @param pattern
	 * @return
	 */
	public static Set<String> keys_lr(String pattern) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			Set<String> allKey = jedis.keys(("*" + pattern + "*"));
			return allKey;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache获取失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	/**
	 * 重命名Redis中的key
	 * @throws Exception
	 */
	public static String reName(String oldKey,String newKey)  {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.rename(oldKey,newKey);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("重命名key失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}



	/**
	 * 往一个hash里面添加多个域
	 * @throws Exception
	 */
	public static Boolean hmset(String key, Map<String, String> map) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.hmset(key,map);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("往hash添加多个域失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	public static Long hincrBy(String key, String field, long value) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.hincrBy(key,field,value);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("往hash添加多个域失败：" + e);
//			return -1L;
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}


	/**
	 * 保存一个string到redis中
	 *
	 * @param key
	 *            键 . <br/>
	 * @param value
	 *            缓存对象 . <br/>
	 * @return true or false .
	 */
	public static Boolean save(String key, String value) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.set(key, value);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache保存失败：" + e);
//			return false;
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	/**
	 * 保存一个string到Redis中(缓存过期时间:使用此工具类中的默认时间) . <br/>
	 * 
	 * @param key
	 *            键 . <br/>
	 * @param value
	 *            缓存对象 . <br/>
	 * @return true or false . <br/>
	 * @throws Exception
	 */
	public static Boolean saveLimitTime(String key, String value) {
		return saveLimitTime(key, value, DEFAULT_CACHE_ONE_HOUR);
	}

	/**
	 * 保存一个对象到redis中并指定过期时间
	 * 
	 * @param key
	 *            键 . <br/>
	 * @param value
	 *            缓存对象 . <br/>
	 * @param seconds
	 *            过期时间（单位为秒）.<br/>
	 * @return true or false .
	 */
	public static Boolean saveLimitTime(String key, String value, int seconds) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.set(key, value);
			jedis.expire(key, seconds);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache保存失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	public static Boolean pipelineSaveLimitTime(Map<String, String> kvs, int seconds) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			Pipeline pipeline = jedis.pipelined();
			for(Map.Entry<String, String> entry : kvs.entrySet()) {
				jedis.set(entry.getKey(), entry.getValue());
				jedis.expire(entry.getKey(), seconds);
			}
			pipeline.sync();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache保存失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	/**
	 * 根据缓存键获取Redis缓存中的值.<br/>
	 * 
	 * @param key
	 *            键.<br/>
	 * @return Object .<br/>
	 * @throws Exception
	 */
	public static String get(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.get(key);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache获取失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	/**
	 * 根据缓存键清除Redis缓存中的值.<br/>
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static Long del(String key) {
		Long rs = null;
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			rs = jedis.del(key);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache删除失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
			return rs;
		}
	}

	/**
	 * 根据缓存键清除Redis缓存中的值.<br/>
	 * 
	 * @param keys
	 * @return
	 * @throws Exception
	 */
	public static Long del(String... keys) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.del(keys);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache删除失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}

	}

	/**
	 * 
	 * @param key
	 * @param seconds
	 *            超时时间（单位为秒）
	 * @return
	 */
	public static Boolean expire(String key, int seconds) {

		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.expire(key, seconds);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache设置超时时间失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	/**
	 * 添加一个内容到指定key的hash中
	 * 
	 * @param key
	 * @param field
	 * @param value
	 * @return
	 */
	public static Boolean addHash(String key, String field, String value) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.hset(key, field, value);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache保存失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}
	/**
	 * 添加一个内容到指定key的hash中
	 *
	 * @param key
	 * @param field
	 * @param value
	 * @return
	 */
	public static Boolean addHashLimitTime(String key, String field, String value, int seconds) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.hset(key, field, value);
			jedis.expire(key,seconds);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache保存失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	/**
	 * 将哈希表 key 中的域 field 的值设置为 value ，当且仅当域 field 不存在。
	 若域 field 已经存在，该操作无效。
	 如果 key 不存在，一个新哈希表被创建并执行 HSETNX 命令。
	 * */
	public static Boolean addHashFieldNotExist(String key, String field, String value) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			long result = jedis.hsetnx(key, field, value);
			return result == 1 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache保存失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	/**
	 * 从指定hash中拿一个对象
	 * 
	 * @param key
	 * @param field
	 * @return
	 */
	public static String getHash(String key, String field) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.hget(key, field);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache读取失败：" + e);
//			return null;
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	/**
	 * 从hash中删除指定filed的值
	 * 
	 * @param key
	 * @param field
	 * @return
	 */
	public static Boolean delHash(String key, String field) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			long result = jedis.hdel(key, field);
			return result == 1 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache删除失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}
	/**
	 * 从hash中删除指定fileds的值
	 *
	 * @param key
	 * @param fields
	 * @return
	 */
	public static Boolean delHash(String key, String... fields) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			long result = jedis.hdel(key, fields);
			return result == 1 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache删除失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	/**
	 * 返回哈希表 key 中的所有域。
	 *
	 * @param key
	 * @return set
	 */
	public static Set<String> getHashFields(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.hkeys(key);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache保存失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}
	/**
	 * 同时将多个 field-value (域-值)对设置到哈希表 key 中。
	 此命令会覆盖哈希表中已存在的域。
	 如果 key 不存在，一个空哈希表被创建并执行 HMSET 操作。
	 *
	 * @param key
	 * @param map
	 * @return
	 */
	public static Boolean addHashMuit(String key, Map<String,String> map ) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.hmset(key,map);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache保存失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	/**
	 * 同时将多个 field-value (域-值)对设置到哈希表 key 中。
	 此命令会覆盖哈希表中已存在的域。
	 如果 key 不存在，一个空哈希表被创建并执行 HMSET 操作。
	 *
	 * @param key
	 * @param map
	 * @param seconds 过期时间
	 * @return
	 */
	public static Boolean addHashMuitLimitTime(String key, Map<String,String> map, int seconds ) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			jedis.hmset(key,map);
			jedis.expire(key,seconds);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache保存失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	/**
	 * 返回哈希表 key 中，一个或多个给定域的值。
	 如果给定的域不存在于哈希表，那么返回一个 nil 值。
	 因为不存在的 key 被当作一个空哈希表来处理，所以对一个不存在的 key 进行 HMGET 操作将返回一个只带有 nil 值的表。
	 *
	 * @param key
	 * @param fields
	 * @return
	 */
	public static List<String> getHashMuti(String key, String... fields) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			List<String> values = jedis.hmget(key, fields);
			return values;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache读取失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	/**
	 * 获得hash中的所有key value
	 * 
	 * @param key
	 * @return
	 */
	public static Map<String, String> getAllHash(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			Map<String,String> map = jedis.hgetAll(key);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache获取失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	/**
	 * 判断一个key是否存在
	 * 
	 * @param key
	 * @return
	 */
	public static Boolean exists(String key) {
		Jedis jedis = null;
		Boolean result = false;
		try {
			jedis = jedisPool.getResource();
			result = jedis.exists(key);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache获取失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

//	/======================set

	/**
	 * 将一个或多个 member 元素加入到集合 key 当中，已经存在于集合的 member 元素将被忽略。
	 假如 key 不存在，则创建一个只包含 member 元素作成员的集合。
	 *
	 * @param key
	 * @param member
	 * @return 被添加到集合中的新元素的数量，不包括被忽略的元素。
	 */
	public static Long sAdd(String key,String... member) {
		Jedis jedis = null;
		Long result = 0L;
		try {
			jedis = jedisPool.getResource();
			result = jedis.sadd(key,member);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("插入setCache失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}


	/**
	 * 返回集合 key 中的所有成员。
	 *
	 * @param key
	 * @return 集合中的所有成员
	 */
	public static Set<String> sGetAll(String key) {
		Jedis jedis = null;
		Set<String> result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.smembers(key);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache获取失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Long zadd(String key, double score, String member) {
		Jedis jedis = null;
		Long result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.zadd(key,score,member);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache:zadd失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Set zrangeByScore(String key, double min, double max) {
		Jedis jedis = null;
		Set result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.zrangeByScore(key,min,max);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache:zrangeByScore失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Set zrange(String key, long start, long end) {
		Jedis jedis = null;
		Set result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.zrange(key,start,end);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache:zrange失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Set zrangeWithScores(String key, long start, long end) {
		Jedis jedis = null;
		Set result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.zrangeWithScores(key,start,end);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache:zrangeByScore失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Long zunionstore(String key, String... sets) {
		Jedis jedis = null;
		Long result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.zunionstore(key,sets);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache:zunionstore失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Long zunionstore(String key, ZParams params, String... sets) {
		Jedis jedis = null;
		Long result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.zunionstore(key,params,sets);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache:zunionstore失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Long zinterstore(String key, ZParams params, String... sets) {
		Jedis jedis = null;
		Long result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.zinterstore(key,params,sets);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache:zunionstore失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Long zinterstoreMAX(String key,String... sets) {
		Jedis jedis = null;
		Long result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.zinterstore(key,new ZParams().aggregate(ZParams.Aggregate.MAX),sets);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache:zunionstore失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Long zinterstoreMIN(String key,String... sets) {
		Jedis jedis = null;
		Long result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.zinterstore(key,new ZParams().aggregate(ZParams.Aggregate.MIN),sets);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache:zunionstore失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Long zunionstoreMAX(String key,String... sets) {
		Jedis jedis = null;
		Long result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.zunionstore(key,new ZParams().aggregate(ZParams.Aggregate.MAX),sets);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache:zunionstore失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Long zunionstoreMIN(String key,String... sets) {
		Jedis jedis = null;
		Long result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.zunionstore(key,new ZParams().aggregate(ZParams.Aggregate.MIN),sets);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache:zunionstore失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Long zcard(String key) {
		Jedis jedis = null;
		Long result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.zcard(key);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache:zcard失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Long zrem(String key,String... members) {
		Jedis jedis = null;
		Long result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.zrem(key,members);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache:zunionstore失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Set<String> zrevrange(String key, long start, long end) {
		Jedis jedis = null;
		Set<String> result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.zrevrange(key,start,end);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache:zrevrange失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	/**
	 * 返回集合 key 中的指定个数的成员。
	 *
	 * @param key
	 * @return 集合中的所有成员
	 */
	public static List<String> srandmember(String key, int count) {
		Jedis jedis = null;
		List<String> result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.srandmember(key,count);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache获取srandmember失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	/**
	 * 返回集合 key 的基数(集合中元素的数量)。
	 *
	 * @param key
	 * @return
	 */
	public static Long sCard(String key) {
		Jedis jedis = null;
		Long result = 0L;
		try {
			jedis = jedisPool.getResource();
			result = jedis.scard(key);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache获取失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	/**
	 * 移除集合 key 中的一个或多个 member 元素，不存在的 member 元素会被忽略。
	 *
	 * @param key
	 * @param member
	 * @return 被成功移除的元素的数量，不包括被忽略的元素。
	 */
	public static Long sDel(String key,String... member) {
		Jedis jedis = null;
		Long result = 0L;
		try {
			jedis = jedisPool.getResource();
			result = jedis.srem(key,member);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache获取失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Boolean sismember(String key,String member) {
		Jedis jedis = null;
		Boolean result = null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.sismember(key,member);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache获取失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}

	}

	//=====================================================
	/**
	 *将 key 中储存的数字值增一。

	 如果 key 不存在，那么 key 的值会先被初始化为 0 ，然后再执行 INCR 操作。

	 如果值包含错误的类型，或字符串类型的值不能表示为数字，那么返回一个错误。

	 本操作的值限制在 64 位(bit)有符号数字表示之内。

	 这是一个针对字符串的操作，因为 Redis 没有专用的整数类型，所以 key 内储存的字符串被解释为十进制 64 位有符号整数来执行 INCR 操作。
	 * @param key
	 * @return 执行 INCR 命令之后 key 的值
	 */
	public static Long incr(String key) {
		Jedis jedis = null;
		Long result = 0L;
		try {
			jedis = jedisPool.getResource();
			result = jedis.incr(key);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache获取失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return result;
	}

	public static Set  sinter(String... keys){
		Jedis jedis = null;
		Set<String> rsSet = null;
		try {
			jedis = jedisPool.getResource();
			rsSet = jedis.sinter(keys);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache求交集失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return rsSet;
	}

	public static Long  sinterstore(String dstkey, String... keys){
		Jedis jedis = null;
		Long rsSet = null;
		try {
			jedis = jedisPool.getResource();
			rsSet = jedis.sinterstore(dstkey,keys);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache求交集失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return rsSet;
	}

	public static Set sdiff(String... keys){
		Jedis jedis = null;
		Set<String> rsSet = null;
		try {
			jedis = jedisPool.getResource();
			rsSet = jedis.sdiff(keys);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache求差集失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return rsSet;
	}

	public static Long  sunionstore(String dataKey, String... keys){
		Jedis jedis = null;
		Long rs = null;
		try {
			jedis = jedisPool.getResource();
			rs = jedis.sunionstore(dataKey,keys);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache求并集并储存失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return rs;
	}

	public static Set sUnion(String... keys){
		Jedis jedis = null;
		Set<String> rsSet = null;
		try {
			jedis = jedisPool.getResource();
			rsSet = jedis.sunion(keys);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Cache求交集失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return rsSet;
	}

	public static Set keys(String pattern){
		Jedis jedis = null;
		Set<String> rsSet = null;
		try {
			jedis = jedisPool.getResource();
			rsSet = jedis.keys(pattern);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("求keys失败：" + e);
			throw e;
		} finally {
			releaseResource(jedis);
		}
		return rsSet;
	}

	public static int getNumActive(){
		try {
			return jedisPool.getNumActive();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("getNumActive失败：" + e);
			throw e;
		}
	}

	public static int getNumIdle(){
		try {
			return jedisPool.getNumIdle();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("getNumIdle失败：" + e);
			throw e;
		}
	}

	public static int getNumWaiters(){
		try {
			return jedisPool.getNumWaiters();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("getNumWaiters失败：" + e);
			throw e;
		}
	}

	public static Long rpush(String key,String... strings) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.rpush(key, strings);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("rpush保存失败：" + e);
//			return false;
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	public static Long lpush(String key,String... strings) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.lpush(key, strings);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("lpush保存失败：" + e);
//			return false;
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	public static String rpop(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.rpop(key);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("rpop失败：" + e);
//			return false;
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	public static String lpop(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.lpop(key);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("lpop保存失败：" + e);
//			return false;
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	public static Long llen(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.llen(key);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("获取llen失败：" + e);
//			return false;
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	public static List<String> lrange(String key, long start, long end) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.lrange(key,start,end);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("获取lrange失败：" + e);
//			return false;
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	public static ScanResult<String> scan(String cursor, ScanParams params) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.scan(cursor,params);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("获取scan失败：" + e);
//			return false;
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

	public static String rpoplpush(String srckey, String dstkey){
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.rpoplpush(srckey,dstkey);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("执行rpoplpush失败：" + e);;
			throw e;
		} finally {
			releaseResource(jedis);
		}
	}

}
