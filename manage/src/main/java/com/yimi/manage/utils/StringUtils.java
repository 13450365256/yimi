package com.yimi.manage.utils;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2017/3/21.
 */

@SuppressWarnings("all")
public class StringUtils {

    /**
     * 字符串补位，num表示总共多少个位
     */
    public static String complementNum(int num, String data) {
        int length = data.length();
        if(length == num) {
            return data;
        } else if(length > num) {
            return null;
        } else {
            int count = num - length;
            StringBuffer sb = new StringBuffer(data);

            for(int i = 0; i < count; i++) {
                sb.insert(0, "0");
            }

            return sb.toString();
        }
    }

    //根据时间戳反转补位
    public static String complementTimestampInversion(long data) {
        if(data < 0){
            return "a" + complementNum(18,data + "");
        }
        return complementNum(19,(Long.MAX_VALUE - data) + "");
    }
    /**
     * sql关键字
     */
    private static String[] sql_key = {"and", "or", "select", "delete", "insert", "update", "where", "group", "order"};

    /**
     * 特殊字符，防止注入式攻击
     */
    private static String[] param_key = {"script", "alert", "XSS", "document", "cookie"};

    /**
     * 日期格式
     */
    public static final String ISO_EXPANDED_DATE_FORMAT = "yyyy-MM-dd";


    /**
     * 是不是数字
     *
     * @param str
     * @return
     */
    public static boolean isNum(String str) {
        return str.matches("^[-+]?(([0-9]+)([.]([0-9]+))?|([.]([0-9]+))?)$");
    }

    /**
     * 验证参数是否包含关键字
     *
     * @param val
     * @return
     */
    public static boolean filterSql(String val) {
        boolean flag = false;
        for (String key : sql_key) {
            if (val.indexOf(key) != -1) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    /**
     * 检查参数是不是带有特殊的字符
     *
     * @param val
     * @return
     */
    public static boolean filterParam(String val) {
        boolean flag = false;
        for (String key : param_key) {
            if (val.indexOf(key) != -1) {
                flag = true;
                break;
            }
        }
        return flag;
    }


    /**
     * 拼接sql in查询
     *
     * @param stringList
     * @return **，**，**
     */
    public static String getListString(List<String> stringList) {
        String str = "";
        for (int i = 0; i < stringList.size(); i++) {
            str += "'" + stringList.get(i) + "'";
            if (i != (stringList.size() - 1)) {
                str += ",";
            }

        }
        return str;
    }

    /**
     * 过滤html代码的特殊字符
     *
     * @param s
     * @return
     */
    public static final String htmlToCode(String s) {
        if (s == null || s.length() == 0) {
            return "";
        } else {
            s = s.replace("\n\r", "<br>&nbsp;&nbsp;");
            s = s.replace("\r\n", "<br>&nbsp;&nbsp;");//这才是正确的！
            s = s.replace("\n", "<br>&nbsp;&nbsp;");
            s = s.replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
            s = s.replace(" ", "&nbsp;");

            s = s.replace("\"", "\\" + "\"");//如果原文含有双引号，这一句最关键！！！！！！
            return s;
        }
    }

    /**
     * 把换行符换成空格符
     *
     * @param s
     * @return
     */
    public static final String filterCode(String s) {
        if (s == null || s.length() == 0) {
            return "";
        } else {
            s = s.replace("\n", "&nbsp;&nbsp;");
            return s;
        }
    }

    /**
     * 字符串转int
     *
     * @param str
     * @return
     */
    public static int strToInt(String str) {
        int iStr = 0;
        if (isNotempty(str)) {
            iStr = Integer.valueOf(str).intValue();
        }
        return iStr;
    }

    /**
     * 字符串转long
     *
     * @param str
     * @return
     */
    public static Long strToLong(String str) {
        long iStr = 0;
        if (isNotempty(str)) {
            iStr = Long.valueOf(str).longValue();
        }
        return new Long(iStr);
    }

    /**
     * 字符串转double
     *
     * @param str
     * @return
     */
    public static double strToDouble(String str) {
        double iStr = 0;
        if (isNotempty(str)) {
            iStr = Double.valueOf(str).doubleValue();
        }
        return iStr;
    }

    /**
     * 字符串转 float
     *
     * @param str
     * @return
     */
    public static float strToFloat(String str) {
        float iStr = 0;
        if (isNotempty(str)) {
            iStr = Float.valueOf(str).floatValue();
        }
        return iStr;
    }

    /**
     * 字符串转 float。
     *
     * @param str       需转换的文本
     * @param zeroCount 补0个数
     * @return
     */
    public static float strToFloat(String str, int zeroCount) {
        if (isEmpty(str)) {
            return 0;
        }
        str = str.trim();//截取掉多余的空格
        StringBuilder result = new StringBuilder();
        // 不用containt方法是因为需要判断是否包含多个点
        if (str.matches("\\d*[.]\\d*")) {
            // 小数位
            String decimalStr = str.substring(str.lastIndexOf(".") + 1);
            result.append(str.replace(".", ""));
            for (; zeroCount > 0; zeroCount--) {
                result.append("0");
            }
            //得到补小数点的位置
            int index = result.length() - decimalStr.length();
            result.insert(index, ".");
        } else {
            result.append(str);
            for (; zeroCount > 0; zeroCount--) {
                result.append("0");
            }
        }
        return Float.parseFloat(result.toString());
    }

    /**
     * 任意数字格式转字符串
     *
     * @param obj
     * @return
     */
    public static String numToString(Object obj) {
        String str = "";
        if (isNotempty(obj)) {
            str = obj.toString().trim();
        }
        return str;
    }

    /**
     * 验证一个字符串或者数组是空
     *
     * @param obj
     * @return
     */
    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        }
        if (obj instanceof List) {
            List list = (List) obj;
            return list.size() == 0;
        } else {
            return obj.toString().trim().length() == 0;
        }
    }

    /**
     * 验证一个字符串或者数组不为空
     *
     * @param obj
     * @return
     */
    public static boolean isNotempty(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof List) {
            List list = (List) obj;
            return list.size() > 0;
        } else {
            return obj.toString().trim().length() > 0;
        }
    }

    /**
     * 判断字符串是否为空(自动截取首尾空白)
     *
     * @param str
     *            源字符串
     * @return
     */
    public static boolean isEmpty(String str) {
        if(str == null){
            return true;
        }else {
            String tmpStr = str.trim();
            return  ("".equals(tmpStr) || tmpStr.equals("NULL") || tmpStr.equals("null"));
        }

    }

    /**
     * 替换字符串函数
     *
     * @param strSource 源字符串
     * @param strFrom   要替换的子串
     * @param strTo     替换为的字符串
     */
    public static String replace(String strSource, String strFrom, String strTo) {
        // 如果要替换的子串为空，则直接返回源串
        if (strFrom == null || strFrom.equals("")) return strSource;
        String strDest = "";
        // 要替换的子串长度
        int intFromLen = strFrom.length();
        int intPos;
        // 循环替换字符串
        while ((intPos = strSource.indexOf(strFrom)) != -1) {
            // 获取匹配字符串的左边子串
            strDest = strDest + strSource.substring(0, intPos);
            // 加上替换后的子串
            strDest = strDest + strTo;
            // 修改源串为匹配子串后的子串
            strSource = strSource.substring(intPos + intFromLen);
        }
        // 加上没有匹配的子串
        strDest = strDest + strSource;
        // 返回
        return strDest;
    }

    /**
     * 简单处理一下日期显示
     * 显示年月日长度，去掉时分秒（例如：2012-12-10,刚好10个字符）
     * 日期为空时显示　"------"
     *
     * @param str
     * @return
     */
    public static String shortDateTools(String str) {
        if (str == null || str.trim().equals("")) {
            str = "------";
        } else {
            str = str.substring(0, 10);
        }
        return str;
    }

    /**
     * 根据时间变量返回时间字符串
     *
     * @param pattern 时间字符串样式
     * @param date    时间变量
     * @return 返回时间字符串
     */
    public static String dateToString(Date date, String pattern) {

        if (date == null) {
            return null;
        }

        try {
            SimpleDateFormat sfDate = new SimpleDateFormat(pattern);
            sfDate.setLenient(false);

            return sfDate.format(date);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 根据时间变量返回时间字符串 yyyy-MM-dd
     *
     * @param date
     * @return String
     */
    public static String dateToString(Date date) {
        return dateToString(date, ISO_EXPANDED_DATE_FORMAT);
    }


    public static <E> String setSqlTitle(Class<E> entity) {
        StringBuffer title = new StringBuffer();
        Field[] fields = entity.getDeclaredFields();

        for (int i = 0; i < fields.length; i++) {
            if (title.length() == 1 & title.length() == i) {
                title.append(fields[i].getName());
            } else {
                title.append(fields[i].getName() + ",");
            }
        }
        return title.toString();
    }

    /**
     * 获取部分sql语句的from后面部分
     *
     * @param sql
     * @return
     */
    public static String setSqlCriteria(String sql) {
        if (sql.length() > 0) {
            return sql.substring(sql.lastIndexOf("from"), sql.length());
        }
        return null;
    }

    /**
     * 返回LigerUI所需要的json
     *
     * @param json
     * @param count
     * @return
     */
    public static String jsonInfo(String json, int count) {
        StringBuffer str = new StringBuffer();
        if (StringUtils.isNotempty(json)) {
            str.append("{\"Rows\":").append(json);
        } else {
            str.append("{\"Rows\":").append("\"\"");
        }
        str.append(", \"Total\":").append(count).append("}");
        return str.toString();
    }

    public static String intToString(int[] ids) {
        StringBuffer info = new StringBuffer();
        if (ids.length > 0) {
            for (int i = 0; i < ids.length; ) {
                if (ids.length == 1 & ids.length == i) {
                    info.append(ids[i]);
                } else {
                    info.append(ids[i]).append(",");
                }
            }
        }
        return null;
    }

    /**
     * 根据date时间 获取前或者向后的每一天的时间数 如2014-05-27  3天前 就会获取 2014-05-26 、2014-05-25
     *
     * @param day    获取多少天
     * @param flag   向前还是向后   1向前 2向后
     * @param date   时间
     * @param Format 格式 如 "yyyy-MM-dd"
     * @return
     */
    public static String[] getDateFormatArray(int day, int flag, Date date, String Format) {
        SimpleDateFormat df = new SimpleDateFormat(Format);
        String dates[] = null;
        dates = new String[day];
        for (int i = 0; i < day; i++) {
            if (flag == 1) {
                dates[i] = df.format(new Date(date.getTime() + (long) i * 24 * 60 * 60 * 1000));
            } else if (flag == 2) {
                dates[i] = df.format(new Date(date.getTime() - (long) i * 24 * 60 * 60 * 1000));
            }
        }
        return dates;
    }

    /**
     * 根据date时间 获取前或者向后数 如2014-05-27  3天前 就会获取 2014-05-26 、2014-05-25
     *
     * @param day    获取多少天
     * @param flag   向前还是向后   1向前 2向后
     * @param date   时间
     * @param Format 格式 如 "yyyy-MM-dd"
     * @return
     */
    public static String getDateFormat(int day, int flag, Date date, String Format) {
        SimpleDateFormat df = new SimpleDateFormat(Format);
        String dates = null;
        if (flag == 0) {
            dates = df.format(new Date(date.getTime()));
        } else if (flag == 1) {
            dates = df.format(new Date(date.getTime() + (long) (day - 1) * 24 * 60 * 60 * 1000));
        } else if (flag == 2) {
            dates = df.format(new Date(date.getTime() - (long) (day - 1) * 24 * 60 * 60 * 1000));
        } else {

        }
        return dates;
    }

    /**
     * 字符串时间 转为天数
     *
     * @param startDate
     * @param endDate
     * @param Format    调用 getDateFormatNum("2014-5-22","2014-5-27","yyyy-MM-dd")
     * @return
     */
    public static long getDateFormatNum(String startDate, String endDate, String Format) {
        SimpleDateFormat df = new SimpleDateFormat(Format);
        long num = 0;
        try {
            Date now = df.parse(endDate);
            Date date = df.parse(startDate);
            long l = now.getTime() - date.getTime();
            num = l / (24 * 60 * 60 * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return num;
    }

    /**
     * 把用逗号分割的字符传进去，返回个数 如：paraNum(12,24,15,23) 返回 4
     *
     * @param mobile
     * @return
     */
    public static int paraNum(String mobile) {
        int praNum = 0;
        if (StringUtils.isNotempty(mobile)) {
            String[] pra = mobile.split(",");
            praNum = pra.length;
        }
        return praNum;
    }

    /**
     * @param mobiles
     * @return
     */
    public static boolean isMobileNO(String mobiles) {
        Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
        Matcher m = p.matcher(mobiles);
        return m.matches();
    }

    /**
     * 6位随机数
     *
     * @return
     */
    public static String sixRandom() {
        int n = 10;
        Random rand = new Random();
        boolean[] bool = new boolean[n];
        int randInt = 0;
        StringBuffer str = new StringBuffer();
        for (int i = 0; i < 6; i++) {
            do {
                randInt = rand.nextInt(n);
            } while (bool[randInt]);
            bool[randInt] = true;
            str.append(randInt);
        }
        return str.toString();
    }


    /**
     * 获取分页语句
     */
    public static String getPagingSql(String paSql, int page, int pageSize) {
        StringBuffer sql = new StringBuffer("select * from( select ttt.*,rownum rn from( ");
        sql.append(paSql);
        int pages = ((page - 1) * pageSize);
        int pageSizes = page * pageSize;
        sql.append("  ) ttt where rownum<= ");
        sql.append(pageSizes);
        sql.append(" ) where rn > ");
        sql.append(pages);
        return sql.toString();
    }



    /**
     * 是否手机号
     *
     * @param phone
     * @return
     */
    public static boolean checkPhone(String phone) {
        Pattern pattern = Pattern.compile("^((13[0-9])|(15[0-9])|(18[0-9])|(14[5,7])|(17[6-8]))\\d{8}$");
        Matcher matcher = pattern.matcher(phone);

        if (matcher.matches()) {
            return true;
        }
        return false;
    }


    /**
     *
     * @Title: getTelnum
     * @Description: TODO(判断字符串是否包含连续7位数字的固话和手机号码，包含返回true)
     * @param  @param sParam
     * @param  @return
     * @return boolean
     */
    public static boolean isContainsTelnum(String Telnum){
        Pattern pattern = Pattern.compile("(?<!\\d)(?:(?:1[358]\\d{9})|(?:861[358]\\d{9}))|(?:\\d{7})(?!\\d)");
        Matcher matcher = pattern.matcher(Telnum);
        if(matcher.find()){
            return true;
        }
        return false;
    }





    /**
     * 检测过滤区域的不正确数值
     *
     * @param areaName
     * @return
     */
    public static boolean checkArea(String areaName) {
        if ("成交房源".equalsIgnoreCase(areaName)) {
            return true;
        } else if ("赠送报纸".equalsIgnoreCase(areaName)) {
            return true;
        } else if ("市外楼盘".equalsIgnoreCase(areaName)) {
            return true;
        } else if ("删除盘源".equalsIgnoreCase(areaName)) {
            return true;
        }
        return false;
    }

    /**
     * 把 , ; 变为 ，；
     *
     * @param str
     * @return
     */
    public static String replStr(String str) {
        str = str.replaceAll(",", "，");
        str = str.replaceAll(";", "；");
        return str.toString();
    }

    /**
     * 驼峰形式转换成下划线
     *
     * @param str
     * @return
     */
    public static String trans(String str) {
        List<Integer> record = new ArrayList<Integer>();
        for (int i = 0; i < str.length(); i++) {
            char tmp = str.charAt(i);

            if ((tmp <= 'Z') && (tmp >= 'A')) {
                record.add(i);//记录每个大写字母的位置
            }

        }
        record.remove(0);//第一个不需加下划线

        str = str.toLowerCase();
        char[] charofstr = str.toCharArray();
        String[] t = new String[record.size()];
        for (int i = 0; i < record.size(); i++) {
            t[i] = "_" + charofstr[record.get(i)];//加“_”
        }
        String result = "";
        int flag = 0;
        for (int i = 0; i < str.length(); i++) {
            if ((flag < record.size()) && (i == record.get(flag))) {
                result += t[flag];
                flag++;
            } else
                result += charofstr[i];
        }

        return result;
    }

    /**
     * 把 String 转为 字符流
     *
     * @param s
     * @return
     */
    public static InputStream getStringInputStream(String s) {
        if (s != null && !s.equals("")) {
            try {
                ByteArrayInputStream stringInputStream = new ByteArrayInputStream(
                        s.getBytes());
                return stringInputStream;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * 解码
     * @param str
     * @return
     */
    public static String encode(String str, String charseName) {
        String charsetName = charseName;
        try {
            return URLDecoder.decode(str, charsetName);
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    /**
     * @Title: entityFiledToSqlFiled
     * @Description: TODO(实体属性名称转换成数据库映射的字段名称)
     * @param str
     * @return 设定文件
     * @return String 返回类型
     * @throws
     */
    public static String entityFiledToSqlFiled(String str){
        if(isEmpty(str)){
            return "";
        }
        return str.replaceAll("[A-Z]", "_$0");
    }

    public static String encodeUTF8(String data){
        String s = "";
        if(data!=null){
            try {
                s = URLEncoder.encode(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return s;
    }

    /**
     * 判断字符串是否是整数
     */
    public static boolean isInteger(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * 判断字符串是否是整数
     */
    public static boolean isLong(String value) {
        try {
            Long.parseLong(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }







    /**
     * 根据字符串拆分结果拼接SQL
     * @param str
     * @param column
     * @param type (int or string)
     * @return
     */
    public static String splitObjAppendSqlEqualValue(String str, String column, String type){
        String [] list = str.split(",");
        StringBuffer sb = new StringBuffer("");
        for (int i=0;i<list.length;i++) {
            if(!isNullString(list[i])){
                sb.append(column);
                if(type.equals("string")){
                    sb.append(" = '");
                    sb.append(list[i]);
                    sb.append("'");
                } else {
                    sb.append(" = ");
                    sb.append(list[i]);
                }
                if (list.length - 1 > i) {
                    sb.append(" or ");
                }
            }
        }
        return sb.toString();
    }

    /**
     * 根据字符串拆分结果拼接SQL
     * @param str
     * @param column
     * @param type (int or string)
     * @return
     */
    public static String splitObjAppendSqlInValue(String str, String column, String type){
        String [] list = str.split(",");
        StringBuffer sb = new StringBuffer("");
        sb.append(column);
        sb.append(" in (");
        for (int i=0;i<list.length;i++) {
            if(!isNullString(list[i])){
                if(type.equals("string")){
                    sb.append("'");
                    sb.append(list[i]);
                    sb.append("'");
                } else {
                    //sb.append(" = ");
                    sb.append(list[i]);
                }
                if (list.length - 1 > i) {
                    sb.append(",");
                }
            }
        }
        sb.append(")");
        return sb.toString();
    }

    /**
     * 根据字符串拆分结果拼接SQL
     * @param str
     * @param column
     * @return
     */
    public static String splitStringAppendSqlEqualValue(String str, String column){
        String [] list = str.split(",");
        StringBuffer sb = new StringBuffer("");
        for (int i=0;i<list.length;i++) {
            if(!isNullString(list[i])){
                sb.append(column);
                sb.append(" = '");
                sb.append(list[i]);
                sb.append("'");
                if (list.length - 1 > i) {
                    sb.append(" or ");
                }
            }
        }
        return sb.toString();
    }

    /**
     * 根据字符串拆分结果拼接模糊匹配SQL
     * @param str
     * @param column
     * @return
     */
    public static String splitStringAppendSqlLikeValue(String str, String column){
        String [] list = str.split(",");
        StringBuffer sb = new StringBuffer("");
        for (int i=0;i<list.length;i++) {
            if(!isNullString(list[i])){
                sb.append(column);
                sb.append(" like ");
                sb.append("'%,");
                sb.append(list[i]);
                sb.append(",%'");
                if (list.length - 1 > i) {
                    sb.append(" or ");
                }
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        String id = ",1,2,3,";
        System.out.println(splitObjAppendSqlInValue(id,"\"create_user_id\"","string"));
        System.out.println(StringUtils.isNum("35㎡"));
    }

    public static boolean isNullString(String str) {
        return str == null || str.trim().isEmpty();
    }



    public static long generateTimestamp(int page, Long timestamp) {
        if(page == 1) {
            return System.currentTimeMillis();
        } else if(timestamp != null) {
            return timestamp.longValue();
        } else {
            return 0L;
        }
    }

    //判断字符串中是否有中文汉字
    public static boolean isContainChinese(String str) {
        Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
        Matcher m = p.matcher(str);
        if (m.find()) {
            return true;
        }
        return false;
    }

    //空对象,null 值转为空字符串
    public static String objNullToStr(Object object){
        if(object == null || object == "null"){
            return "";
        }
        return object.toString();
    }

    //客户需求类型转换
    public static String customerNeedTypeToStr(String str){
        if(isNotempty(str)){
            String [] list = str.split(",");
            String content = "";
            for(int i=0;i<list.length;i++){
                String type = list[i];
                if(StringUtils.isNotempty(type)){
                    switch(type.trim()){
                        case "01":
                            content += "购房";
                            break;
                        case "02":
                            content += "租房";
                            break;
                        default:
                            content += "其它";
                            break;
                    }
                    if(list.length - 1 > i){
                        content += ",";
                    }
                }
            }
            return content;
        }
        return "";
    }

    //客户类型转换
    public static String customerVipTypeToStr(String str){
        if(isNotempty(str)){
            String content = "";
            switch(str){
                case "01":
                    content = "一般客";
                   break;
                case "02":
                    content = "观望客";
                    break;
                case "03":
                    content = "重要客";
                    break;
                default:
                    content = "其它";
                    break;
            }
            return content;
        }
        return "";
    }

    //生成Hbase日期排序值
    public static Long getHbaseDateAsc(Date date){
        Long dateValue = Long.MAX_VALUE;
        if(StringUtils.isNotempty(date)){
            dateValue = Long.MAX_VALUE - date.getTime();
            if(dateValue < 0){
                dateValue = Long.MAX_VALUE;
            }
        }
        return dateValue;
    }



    //转换有小数点的数值
    public static String numberFormatToStr(Object object){
        String str = "";
        if(object == null || object == "null"){
            return str;
        }
        //NumberFormat nf = NumberFormat.getInstance();
        //NumberFormat nf1=NumberFormat.getPercentInstance(Locale.CANADA);
        //nf1.setMaximumFractionDigits(1);
        DecimalFormat nf = new DecimalFormat("#");
        //有小数点只保留1位
        nf.setMaximumFractionDigits(1);
        try {
            str = nf.format(object);
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
        return str;
    }

    // ================================================================================
    // UUID 和 顺序号生成
    // ================================================================================
    // generate UUID
    public static String getUUID() {
        String uuid = UUID.randomUUID().toString();
        return uuid.replaceAll("-", "");
    }


    //布尔值转换
    public static String zyBooleanConvertToStr(String info){
        String str = "";
        switch (info) {
            case "T":
                str = "true";
                break;
            case "F":
                str = "false";
                break;
            case "false":
                str = "false";
                break;
            case "true":
                str = "true";
                break;
            default:
                 str = "";
                 break;
        }
        return str;
    }
}
