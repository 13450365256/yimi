package com.yimi.manage.service;

import java.io.File;

/**
 * Created by Administrator on 2017/12/21.
 */
public interface NuoTaiService {

    String buildYimiOrderExcel(File file,String giftNum) throws Exception;

    void buildPriceRedis() throws Exception;
}
