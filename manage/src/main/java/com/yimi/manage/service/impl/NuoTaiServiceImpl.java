package com.yimi.manage.service.impl;

import com.yimi.manage.constant.ManageConstant;
import com.yimi.manage.constant.NuoTaiConstant;
import com.yimi.manage.constant.RedisConstant;
import com.yimi.manage.service.NuoTaiService;
import com.yimi.manage.system.YiMiApplicationContext;
import com.yimi.manage.utils.ObjectUtils;
import com.yimi.manage.utils.StringUtils;
import com.yimi.manage.utils.redis.RedisStringUtils;
import com.yimi.manage.vo.nuoTai.NuoTaiYiMiOrderVo;
import com.yimi.manage.vo.nuoTai.NuoTaiYunFanOrderVo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Administrator on 2017/12/21.
 */
@Service("nuoTaiService")
public class NuoTaiServiceImpl implements NuoTaiService {

    SimpleDateFormat sdfdt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private Logger logger = LogManager.getLogger(this.getClass());

    @Resource
    YiMiApplicationContext yiMiApplicationContext;

    @Override
    public String buildYimiOrderExcel(File file,String giftNum) throws Exception{
        List<NuoTaiYiMiOrderVo> yiMiOrderVoList = new ArrayList<NuoTaiYiMiOrderVo>();
        InputStream inputStream = null;
        try {
            if (file.exists()) {
                //创建Excel,读取文件内容
                inputStream = new FileInputStream(file);
                HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
                HSSFSheet sheet = workbook.getSheetAt(0);

                int lastRowNum = sheet.getLastRowNum();
//                Map<String,Double> priceMap = getPriceMap();
//                if(priceMap != null && priceMap.size() > 0) {
                    for (int i = 1; i <= lastRowNum; i++) {
                        HSSFRow row = sheet.getRow(i);
                        NuoTaiYunFanOrderVo yunFanOrderVo = buildYunFanOrderVoByExcelRow(row);
                        if (yunFanOrderVo != null) {
                            NuoTaiYiMiOrderVo yiMiOrderVo = new NuoTaiYiMiOrderVo();
                            ObjectUtils.copyAll(yunFanOrderVo, yiMiOrderVo);
                            if(StringUtils.isNotempty(yiMiOrderVo.getGoodsModelNumber())) {
                                String buyingPriceStr = RedisStringUtils.getHash(RedisConstant.NUO_TAI_PRICE_HASH,yiMiOrderVo.getGoodsModelNumber());
                                if(buyingPriceStr != null) {
                                    Double buyingPrice = Double.parseDouble(buyingPriceStr);
                                    if(buyingPrice != null && buyingPrice > 0) {
                                        if(yunFanOrderVo.getUnitPrice() * yunFanOrderVo.getQuantity() == yunFanOrderVo.getAmount()){
                                            yiMiOrderVo.setBuyingPrice(buyingPrice * yunFanOrderVo.getQuantity());
                                        }else {
                                            yiMiOrderVo.setBuyingPrice(buyingPrice);
                                        }
                                    }else {
                                        yiMiOrderVo.setBuyingPrice(-100000000.0);
                                    }
                                }else {
                                    System.out.println("buildYimiOrderExcel:没有阿里巴巴价:" + yiMiOrderVo.getGoodsModelNumber());
                                    yiMiOrderVo.setBuyingPrice(-100000000.0);
                                }
                            }else {
                                yiMiOrderVo.setBuyingPrice(-100000000.0);
                            }
                            yiMiOrderVo.setGoodsModelNumber(yiMiOrderVo.getGoodsModelNumber() + ",尺码:" + yiMiOrderVo.getSize());
                            yiMiOrderVoList.add(yiMiOrderVo);
                            if(StringUtils.isNotempty(giftNum)) {
                                NuoTaiYiMiOrderVo giftYiMiOrderVo = setGift(giftNum);
                                giftYiMiOrderVo.setOrderNo(yunFanOrderVo.getOrderNo());
                                giftYiMiOrderVo.setPayTime(yunFanOrderVo.getPayTime());
                                giftYiMiOrderVo.setContacts(yunFanOrderVo.getContacts());
                                giftYiMiOrderVo.setContactAddress(yunFanOrderVo.getContactAddress());
                                giftYiMiOrderVo.setContactNumber(yunFanOrderVo.getContactNumber());
                                giftYiMiOrderVo.setStatus1(yunFanOrderVo.getStatus1());
                                giftYiMiOrderVo.setStatus2(yunFanOrderVo.getStatus2());
                                yiMiOrderVoList.add(giftYiMiOrderVo);
                            }
                        }
                    }
                    if(yiMiOrderVoList.size() > 0){
                        return exportYimiOrderExcel(yiMiOrderVoList);
                    }else {
                        return null;
                    }

            }else {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
            throw e;
        }finally {
            if (inputStream != null){
                inputStream.close();
            }
        }
    }

    private NuoTaiYiMiOrderVo setGift(String giftNum){
        NuoTaiYiMiOrderVo giftYiMiOrderVo = new NuoTaiYiMiOrderVo();
        giftYiMiOrderVo.setCategory("居家日用");
        giftYiMiOrderVo.setSupplier("广州一米商贸有限公司");
        giftYiMiOrderVo.setBrand("诺泰");
        giftYiMiOrderVo.setGoodsName("j01护膝");
        giftYiMiOrderVo.setGoodsModelNumber("j01");
        giftYiMiOrderVo.setColor("图片色");
        giftYiMiOrderVo.setSize("L");
        giftYiMiOrderVo.setBarCode("j01");
        giftYiMiOrderVo.setBuyingPrice(3.0);
        giftYiMiOrderVo.setUnitPrice(28.0);
        giftYiMiOrderVo.setQuantity(1);
        giftYiMiOrderVo.setAmount(28.0);

        return  giftYiMiOrderVo;
    }

    private String exportYimiOrderExcel(List<NuoTaiYiMiOrderVo> yiMiOrderVoList) throws Exception {
        XSSFWorkbook workbook = null;
        if (yiMiOrderVoList.size() > 0) {
            workbook = new XSSFWorkbook();

            CellStyle yellowStyle = workbook.createCellStyle();
            setCellAllBorder(yellowStyle);
            setCellColor(yellowStyle,IndexedColors.YELLOW.getIndex());

            CellStyle blueCenterStyle = workbook.createCellStyle();
            setCellAllBorder(blueCenterStyle);
            setCellColor(blueCenterStyle,IndexedColors.AQUA.getIndex());
            setCellVerticalAlignment(blueCenterStyle,VerticalAlignment.CENTER);
            setCellAlignment(blueCenterStyle,HorizontalAlignment.CENTER);

            CellStyle allBorderStyle = workbook.createCellStyle();
            setCellAllBorder(allBorderStyle);

            XSSFSheet sheet = workbook.createSheet("sheet1");
            sheet.setDefaultColumnWidth(25);
            sheet.setColumnWidth(0,3940);
            sheet.setColumnWidth(15,3640);

            createYimiOrderExcelTitle(sheet,blueCenterStyle);

            for (int i = 0; i < yiMiOrderVoList.size(); i++) {

                NuoTaiYiMiOrderVo yiMiOrderVo = yiMiOrderVoList.get(i);
                String allAdd = yiMiOrderVo.getContactAddress();
                String sheng = null;
                String shi = null;
                String qu = null;
                String add = null;
                int currentIndex = -1;
                if(StringUtils.isNotempty(allAdd)) {
                    int shengIndex = allAdd.indexOf("省");
                    if(shengIndex >= 0) {
                        currentIndex = shengIndex;
                        sheng = allAdd.substring(0,shengIndex + 1);
                        int shiIndex = allAdd.indexOf("市");
                        if(shiIndex >= 0){
                            currentIndex = shiIndex;
                            shi = allAdd.substring(shengIndex + 1,shiIndex + 1);
                            int quIndex = allAdd.indexOf("区");
                            if(quIndex >= 0){
                                currentIndex = quIndex;
                                qu = allAdd.substring(shiIndex + 1,quIndex + 1);
                            }
                        }
                    }
                }
                if(currentIndex >= 0) {
                    add = allAdd.substring(currentIndex + 1);
                }else {
                    add = allAdd;
                }
                XSSFRow row = sheet.createRow(i + 1);
                for (int j = 0; j < NuoTaiConstant.YIMI_ORDER_EXCEL_TITLE.length; j++) {
                    XSSFCell cell = row.createCell(j);
                    cell.setCellStyle(allBorderStyle);
                    switch (j) {
/*                        case 0:
                            cell.setCellValue(yiMiOrderVo.getOrderNo());
                            break;*/

                        case 1:
//                            cell.setCellValue(sdfdt.format(yiMiOrderVo.getPayTime()));
                            cell.setCellValue("云返服饰");
                            break;

                        /*case 2:
                            cell.setCellValue(yiMiOrderVo.getCategory());
                            break;

                        case 3:
                            cell.setCellValue(yiMiOrderVo.getBrand());
                            break;*/

                        case 4:
//                            cell.setCellValue(yiMiOrderVo.getGoodsName());
                            cell.setCellValue(yiMiOrderVo.getContacts());
                            break;

                        case 5:
//                            cell.setCellValue(yiMiOrderVo.getGoodsModelNumber());
                            if(sheng != null) {
                                cell.setCellValue(sheng);
                            }
                            break;

                        case 6:
//                            cell.setCellValue(yiMiOrderVo.getColor());
                            if(shi != null){
                                cell.setCellValue(shi);
                            }
                            break;

                        case 7:
//                            cell.setCellValue(yiMiOrderVo.getSize());
                            if(qu != null){
                                cell.setCellValue(qu);
                            }
                            break;

                        case 8:
//                            cell.setCellValue(yiMiOrderVo.getBarCode());
                            if(add != null){
                                cell.setCellValue(add);
                            }
                            break;

                        case 9:
//                            cell.setCellStyle(yellowStyle);
//                            cell.setCellValue(yiMiOrderVo.getBuyingPrice());
                            cell.setCellValue(yiMiOrderVo.getContactNumber());
                            break;

                        /*case 10:
                            cell.setCellValue(yiMiOrderVo.getUnitPrice());
                            break;

                        case 11:
                            cell.setCellValue(yiMiOrderVo.getQuantity());
                            break;

                        case 12:
                            cell.setCellValue(yiMiOrderVo.getAmount());
                            break;

                        case 13:
                            cell.setCellValue(yiMiOrderVo.getContacts());
                            break;*/

                        case 14:
//                            cell.setCellValue(yiMiOrderVo.getContactAddress());
                            cell.setCellValue(yiMiOrderVo.getGoodsModelNumber());
                            break;

                        case 15:
//                            cell.setCellValue(yiMiOrderVo.getContactNumber());
                            cell.setCellStyle(yellowStyle);
                            cell.setCellValue(yiMiOrderVo.getBuyingPrice());
                            break;

                        case 16:
//                            cell.setCellStyle(yellowStyle);
                            cell.setCellValue(yiMiOrderVo.getQuantity());
                            break;

                        /*case 17:
                            cell.setCellStyle(yellowStyle);
                            break;

                        case 18:
                            cell.setCellStyle(yellowStyle);
                            break;

                        case 19:
                            cell.setCellValue(yiMiOrderVo.getStatus1());
                            break;

                        case 20:
                            cell.setCellValue(yiMiOrderVo.getStatus2());
                            break;
*/

                    }

                }
            }
            createYimiOrderExcelTotalPrice(sheet,yellowStyle,getTotalPrice(yiMiOrderVoList));
            ByteArrayOutputStream os = new ByteArrayOutputStream();

            if (workbook != null) {
                workbook.write(os);
                String dateStr = sdf.format(new Date());
                //Excel文件生成后存储的位置。

                String absDirPath =  yiMiApplicationContext.getApplicationContext()
                        .getResource(NuoTaiConstant.NUO_TAI_YI_MI_ORDERS_EXCEL_PATH).getFile().getAbsolutePath() + "/" + dateStr;

                System.out.println("absDirPath:" + absDirPath);

                File dirFile = new File(absDirPath);
                if(!dirFile.exists()) {
                    dirFile.mkdirs();
                }


                String outFName = "云返服饰订单信息" + dateStr + ".xlsx";
                File file = new File(absDirPath + "/" + outFName);
                OutputStream fos  = null;
                try
                {
                    fos = new FileOutputStream(file);
                    workbook.write(fos);

                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    if(os != null){
                        os.close();
                    }
                    if(fos != null){
                        fos.close();
                    }
                }
               /* String outPath = ManageConstant.SERVER_PATH + "excel/nuoTai/orders/yiMiOrders/" + dateStr + "/";
                String outFName = "云返服饰订单信息" + dateStr + ".xlsx";
                String inPath = dirPath + outFName;
                System.out.println("inPath:" + inPath + ",outPath:" + outPath + ",outFName:" + outFName);
                copyToServer(inPath, outPath,outFName);*/

//                return NuoTaiConstant.WEB_NUO_TAI_YI_MI_ORDERS_EXCEL_PATH  + dateStr + "/" + outFName;
                String rsUrl = NuoTaiConstant.WEB_NUO_TAI_YI_MI_ORDERS_EXCEL_PATH  + dateStr + "/" + outFName;
                System.out.println("rsUrl:" + rsUrl);
                return rsUrl;
            }else {
                return null;
            }

        }else {
            return null;
        }
    }

    private void copyToServer(String inPathFile,String outPath,String fileName) throws Exception{
        FileInputStream sfis = null;
        FileOutputStream sfos = null;
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            File file = new File(outPath);
            if(!file.exists()){
                file.mkdirs();
            }
            sfis = new FileInputStream(inPathFile);
            sfos = new FileOutputStream(outPath + fileName);
            bis = new BufferedInputStream(sfis);
            bos = new BufferedOutputStream(sfos);
            int by = 0;
            byte[] buf = new byte[1024];
            while ((by = bis.read(buf)) != -1) {
                bos.write(buf, 0, by);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(bis != null){
                bis.close();
            }
            if(bos != null){
                bos.close();
            }
            if(sfis != null){
                sfis.close();
            }
            if(sfos != null){
                sfos.close();
            }

        }
    }

    private void createYimiOrderExcelTotalPrice(XSSFSheet sheet,CellStyle style,Double totalPrice){
        XSSFRow row = sheet.createRow(sheet.getLastRowNum() + 1);
        XSSFCell cell = row.createCell(15);
        cell.setCellValue(totalPrice);
        cell.setCellStyle(style);
    }

    private Double getTotalPrice(List<NuoTaiYiMiOrderVo> voList){
        double totalPrice = 0;
        for(int i=0;i<voList.size();i++){
            totalPrice += voList.get(i).getBuyingPrice()==null?0:voList.get(i).getBuyingPrice();
        }
        return totalPrice;
    }

    private void createYimiOrderExcelTitle(XSSFSheet sheet,CellStyle style){
        XSSFRow row = sheet.createRow(0);
        row.setHeight((short) 600);
        for(int i=0;i<NuoTaiConstant.YIMI_ORDER_EXCEL_TITLE.length;i++){
            XSSFCell cell = row.createCell(i);

            cell.setCellValue(NuoTaiConstant.YIMI_ORDER_EXCEL_TITLE[i]);
            cell.setCellStyle(style);
        }
    }

    private Map<String,Double> getPriceMap() throws Exception{
        Map<String,Double> rsMap = new HashMap<String,Double>();
        InputStream inputStream = null;
        try {
            File file = new File(NuoTaiConstant.NUO_TAI_PRICE_EXCEL_PATH + "诺泰报价表.xlsx");
            if (file.exists()) {
                inputStream = new FileInputStream(file);
                XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
                XSSFSheet sheet = workbook.getSheetAt(0);
                int lastRowNum = sheet.getLastRowNum();
                for(int i=1;i<=lastRowNum;i++){
                    XSSFRow row = sheet.getRow(i);
                    XSSFCell keyCell = row.getCell(0);
                    String key = keyCell.getStringCellValue();
                    XSSFCell valueCell = row.getCell(1);
                    Double value = valueCell.getNumericCellValue();
                    if(StringUtils.isNotempty(key) && value != null){
                        rsMap.put(key.trim(),value);
                    }
                }

            }else {
                System.out.println("getPriceMap:" + file.getPath() + "   不存在文件或文件夹");
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
            throw e;
        }finally {
            if(inputStream != null){
                inputStream.close();;
            }
        }



        return rsMap;
    }

    private NuoTaiYunFanOrderVo buildYunFanOrderVoByExcelRow(HSSFRow row) throws Exception{
        short lastCellNum = row.getLastCellNum();
        NuoTaiYunFanOrderVo vo = null;
        for(int i=0;i<lastCellNum;i++){
            HSSFCell cell = row.getCell(i);
            String content = cell.getStringCellValue();
            if(StringUtils.isNotempty(content)){
                content = content.trim();
                switch (i) {
                    case 0:
                        vo = new NuoTaiYunFanOrderVo();
                        vo.setOrderNo(content);
                        break;

                    case 1:
                        vo.setPayTime(sdfdt.parse(content));
                        break;

                    case 2:
                    vo.setCategory(content);
                    break;

                    case 3:
                    vo.setSupplier(content);
                    break;

                    case 4:
                    vo.setBrand(content);
                    break;

                    case 5:
                    vo.setGoodsName(content);
                    break;

                    case 6:
                    vo.setGoodsModelNumber(content);
                    break;

                    case 7:
                    vo.setColor(content);
                    break;

                    case 8:
                    vo.setSize(content);
                    break;

                    case 9:
                    vo.setBarCode(content);
                    break;

                    case 10:
                    vo.setBuyingPrice(Double.parseDouble(content));
                    break;

                    case 11:
                    vo.setUnitPrice(Double.parseDouble(content));
                    break;

                    case 12:
                    vo.setQuantity(Integer.parseInt(content));
                    break;

                    case 13:
                    vo.setAmount(Double.parseDouble(content));
                    break;

                    case 14:
                    vo.setContacts(content);
                    break;

                    case 15:
                    vo.setContactAddress(content);
                    break;

                    case 16:
                    vo.setContactNumber(content);
                    break;

                    case 17:
                    vo.setExpressCompany(content);
                    break;

                    case 18:
                    vo.setLogisticsNumber(content);
                    break;

                    case 19:
                    vo.setDeliveryTime(sdfdt.parse(content));
                    break;

                    case 20:
                    vo.setStatus1(content);
                    break;

                    case 21:
                    vo.setStatus2(content);
                    break;
                }
            }
        }
        return vo;
    }

    private void setCellAllBorder(CellStyle cellStyle){

        cellStyle.setBorderBottom(BorderStyle.THIN); //下边框
        cellStyle.setBorderLeft(BorderStyle.THIN);//左边框
        cellStyle.setBorderTop(BorderStyle.THIN);//上边框
        cellStyle.setBorderRight(BorderStyle.THIN);//右边框

    }

    private void setCellColor(CellStyle cellStyle,short bg){
        cellStyle.setFillForegroundColor(bg);
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

    }

    private void setCellVerticalAlignment(CellStyle cellStyle,VerticalAlignment verticalAlignment){
        cellStyle.setVerticalAlignment(verticalAlignment);

    }

    private void setCellAlignment(CellStyle cellStyle,HorizontalAlignment horizontalAlignment){
        cellStyle.setAlignment(horizontalAlignment);

    }

    public void buildPriceRedis() throws Exception{
        InputStream inputStream = null;
        try {
//            File file = new File(NuoTaiConstant.NUO_TAI_PRICE_EXCEL_PATH + "诺泰报价表.xlsx");
            org.springframework.core.io.Resource resource = yiMiApplicationContext.getApplicationContext()
                    .getResource(NuoTaiConstant.NUO_TAI_PRICE_EXCEL_PATH + "诺泰报价表.xlsx");
            File file = resource.getFile();
            if (file.exists()) {
                inputStream = new FileInputStream(file);
                XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
                XSSFSheet sheet = workbook.getSheetAt(0);
                int lastRowNum = sheet.getLastRowNum();
                for(int i=1;i<=lastRowNum;i++){
                    XSSFRow row = sheet.getRow(i);
                    XSSFCell keyCell = row.getCell(0);
                    if(keyCell == null){
                        continue;
                    }
                    String key = keyCell.getStringCellValue().trim();
                    XSSFCell valueCell = row.getCell(1);
                    if(valueCell == null){
                        continue;
                    }
                    Double newValue = valueCell.getNumericCellValue();
                    if(StringUtils.isNotempty(key) && newValue != null){
                        key = key.trim();
                        String oldValueStr = RedisStringUtils.getHash(RedisConstant.NUO_TAI_PRICE_HASH,key);
                        if(StringUtils.isNotempty(oldValueStr)){
                            double oldValue = Double.parseDouble(oldValueStr);
                            if(newValue < oldValue) {
                                RedisStringUtils.addHash(RedisConstant.NUO_TAI_PRICE_HASH, key,newValue + "");
                            }
                        }else {
                            RedisStringUtils.addHash(RedisConstant.NUO_TAI_PRICE_HASH, key,newValue + "");
                        }

                    }
                }

            }else {
                System.out.println("buildPriceRedis:" + file.getPath() + "   不存在文件或文件夹");
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
            throw e;
        }finally {
            if(inputStream != null){
                inputStream.close();;
            }
        }
    }
}
