package com.yimi.test.elasticJob;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.dangdang.ddframe.job.api.simple.SimpleJob;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class DataflowDemoJob implements DataflowJob<String> {

    @Override
    public List<String> fetchData(ShardingContext context) {
        List<String> list = new ArrayList<>();
        list.add(UUID.randomUUID().toString());
        list.add(UUID.randomUUID().toString());
        System.out.println("分片数量:"+context.getShardingTotalCount()
                +",当前分区:"+context.getShardingItem()
                + ",抓取到数据：" + list);
        return list;
    }

    @Override
    public void processData(ShardingContext context, List data) {
        System.out.println(context.getShardingItem() + "处理数据:" + data);
    }
}