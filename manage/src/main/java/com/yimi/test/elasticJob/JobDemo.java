package com.yimi.test.elasticJob;

import com.dangdang.ddframe.job.config.JobCoreConfiguration;
import com.dangdang.ddframe.job.config.simple.SimpleJobConfiguration;
import com.dangdang.ddframe.job.lite.api.JobScheduler;
import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;

public class JobDemo {

    public static void main(String[] args) {
        new JobScheduler(createRegistryCenter(), createJobConfiguration()).init();
    }

    //配置注册中心
    private static CoordinatorRegistryCenter createRegistryCenter() {
        //配置zk地址，命名空间
        CoordinatorRegistryCenter regCenter = new ZookeeperRegistryCenter(new ZookeeperConfiguration("172.16.8.184:2181", "elastic-job-simple-demo"));
        regCenter.init();
        return regCenter;
    }

    //配置job
    private static LiteJobConfiguration createJobConfiguration() {
        //配置任务名称， 执行计划， 分片数量
        JobCoreConfiguration jobCoreConfiguration = JobCoreConfiguration.newBuilder("SimpleJobDemo", "0/5 * * * * ?", 10).build();
        //配置任务类型
        SimpleJobConfiguration jobConfiguration = new SimpleJobConfiguration(jobCoreConfiguration, SimpleJobDemo.class.getCanonicalName());
        LiteJobConfiguration liteJobConfiguration = LiteJobConfiguration.newBuilder(jobConfiguration).overwrite(true).build();
        return liteJobConfiguration;
    }

}
