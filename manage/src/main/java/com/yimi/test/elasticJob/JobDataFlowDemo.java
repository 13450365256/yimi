package com.yimi.test.elasticJob;

import com.dangdang.ddframe.job.config.JobCoreConfiguration;
import com.dangdang.ddframe.job.config.dataflow.DataflowJobConfiguration;
import com.dangdang.ddframe.job.lite.api.JobScheduler;
import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;

public class JobDataFlowDemo {

    public static void main(String[] args) {
        new JobScheduler(createRegistryCenter(), createJobConfiguration()).init();
    }

    //配置注册中心
    private static CoordinatorRegistryCenter createRegistryCenter() {
        CoordinatorRegistryCenter regCenter = new ZookeeperRegistryCenter(new ZookeeperConfiguration("172.16.8.184:2181", "elastic-job-dataflow-demo"));
        regCenter.init();
        return regCenter;
    }

    //配置job
    private static LiteJobConfiguration createJobConfiguration() {
        JobCoreConfiguration jobCoreConfiguration = JobCoreConfiguration.newBuilder("dataflowDemoJob", "0/5 * * * * ?", 5).build();
        Boolean streamingProcess = true;
        DataflowJobConfiguration dataflowJobConfiguration = new DataflowJobConfiguration(jobCoreConfiguration, DataflowDemoJob.class.getCanonicalName(), streamingProcess);

        // 定义Lite作业根配置
        LiteJobConfiguration liteJobConfiguration = LiteJobConfiguration.newBuilder(dataflowJobConfiguration).overwrite(true).build();

        return liteJobConfiguration;
    }

}
