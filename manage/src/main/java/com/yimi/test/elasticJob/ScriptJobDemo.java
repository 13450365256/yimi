package com.yimi.test.elasticJob;

import com.dangdang.ddframe.job.config.JobCoreConfiguration;
import com.dangdang.ddframe.job.config.script.ScriptJobConfiguration;
import com.dangdang.ddframe.job.lite.api.JobScheduler;
import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermissions;

public class ScriptJobDemo {

    public static void main(String[] args) throws IOException {
        new JobScheduler(createRegistryCenter(), createJobConfiguration()).init();
    }

    //配置注册中心
    private static CoordinatorRegistryCenter createRegistryCenter() {
        CoordinatorRegistryCenter regCenter = new ZookeeperRegistryCenter(new ZookeeperConfiguration("172.16.8.184:2181", "elastic-job-script-demo"));
        regCenter.init();
        return regCenter;
    }

    //配置job
    private static LiteJobConfiguration createJobConfiguration() throws IOException {
        JobCoreConfiguration coreConfiguration = JobCoreConfiguration.newBuilder("scriptDemoJob", "0/5 * * * * ?", 1).build();
        ScriptJobConfiguration scriptJobConfiguration = new ScriptJobConfiguration(coreConfiguration, buildScriptCommandLine());
        LiteJobConfiguration liteJobConfiguration = LiteJobConfiguration.newBuilder(scriptJobConfiguration).overwrite(true).build();
        return liteJobConfiguration;
    }

    private static String buildScriptCommandLine() throws IOException {
        //判断当前系统
        if (System.getProperties().getProperty("os.name").contains("Windows")) {
            return Paths.get(ScriptJobDemo.class.getResource("/script/demo.bat").getPath().substring(1)).toString();
        }
        Path result = Paths.get(ScriptJobDemo.class.getResource("/script/demo.sh").getPath());
        Files.setPosixFilePermissions(result, PosixFilePermissions.fromString("rwxr-xr-x"));
        return result.toString();
    }

}
