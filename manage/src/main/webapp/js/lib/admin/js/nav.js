var navs = [{
	"title" : "后台首页",
	"icon" : "icon-computer",
	"href" : ctx+"/admin/admin_index.do",
	"spread" : true
},{
	"title" : "订单管理",
	"icon" : "icon-text",
	"href" : ctx+"/admin/admin_order_index.do",
	"spread" : true
},{
	"title" : "任务管理",
	"icon" : "icon-text",
	"href" : ctx+"/admin/admin_job_index.do",
	"spread" : true
},{
	"title" : "系统基本参数",
	"icon" : "icon-text",
	"href" : "",
	"spread" : true
},{
	"title" : "二级菜单演示",
	"icon" : "icon-caidan",
	"href" : "",
	"spread" : true,
	"children" : [
		{
			"title" : "二级菜单1",
			"icon" : "icon-text",
			"href" : "",
			"spread" : true
		},
		{
			"title" : "二级菜单2",
			"icon" : "icon-text",
			"href" : "",
			"spread" : true
		}
	]
}]